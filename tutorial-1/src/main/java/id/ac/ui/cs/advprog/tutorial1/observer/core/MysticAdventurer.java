package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        //ToDo: Complete Me
        this.guild = guild;
    }

    //ToDo: Complete Me

    @Override
    public void update() {
        System.out.println("mystic");
        if(guild.getQuestType().equalsIgnoreCase("E") ||
                guild.getQuestType().equalsIgnoreCase("D")){
            this.getQuests().add(guild.getQuest());
        }
    }
}
