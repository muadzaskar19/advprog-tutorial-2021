package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
    //ToDo: Complete me
    public AgileAdventurer() {
        AttackWithGun attackWithGun = new AttackWithGun();
        DefendWithBarrier defendWithBarrier = new DefendWithBarrier();
        this.setAttackBehavior(attackWithGun);
        this.setDefenseBehavior(defendWithBarrier);
    }



    @Override
    public String attack() {
        return this.attack();
    }

    @Override
    public String defend() {
        return this.defend();
    }

    @Override
    public String getAlias() {
        return "Agile Adventurer";
    }
}
