package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
    //ToDo: Complete me
    public KnightAdventurer(){
        AttackWithSword pedang = new AttackWithSword();
        DefendWithArmor armor = new DefendWithArmor();
        this.setAttackBehavior(pedang);
        this.setDefenseBehavior(armor);
    }

    @Override
    public String defend() {
        return this.defend();
    }

    @Override
    public String attack() {
        return this.attack();
    }

    @Override
    public String getAlias() {
        return "Knight Adventurer";
    }
}
