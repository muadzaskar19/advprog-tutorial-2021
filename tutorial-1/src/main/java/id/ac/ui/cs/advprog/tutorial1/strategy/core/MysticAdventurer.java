package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {
    //ToDo: Complete me

    public  MysticAdventurer(){
        AttackWithMagic magic = new AttackWithMagic();
        DefendWithShield shield = new DefendWithShield();
        this.setAttackBehavior(magic);
        this.setDefenseBehavior(shield);
    }

    @Override
    public String getAlias() {
        return "Mystic Adventurer";
    }

    @Override
    public String defend() {
        return this.attack();
    }

    @Override
    public String attack() {
        return this.defend();
    }
}
