package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;
import java.util.Collections;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    ArrayList<Spell> chainSpellArrayList;
    public ChainSpell(ArrayList<Spell> spellArrayList){
        chainSpellArrayList = spellArrayList;
    }
    @Override
    public void cast(){
        for(Spell x : chainSpellArrayList){
            x.cast();
        }
    }
    @Override
    public void undo(){
        System.out.println("Chain Spell Undo");
        for(int i = chainSpellArrayList.size()-1 ; i>=0;i--){
            chainSpellArrayList.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
