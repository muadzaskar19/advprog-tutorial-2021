package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {

    private Map<String, Spell> spells;
    protected Spell latestSpell;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
        latestSpell = spell;
    }

    public void castSpell(String spellName) {
        // TODO: Complete
        spells.get(spellName).cast();
    }

    public void undoSpell() {
        // TODO: Complete Me
        latestSpell.undo();
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
