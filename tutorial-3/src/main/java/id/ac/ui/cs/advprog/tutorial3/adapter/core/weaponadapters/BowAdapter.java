package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;

// TODO: complete me :)

public class BowAdapter implements Weapon {
    private Bow bow;
    Boolean aimShoot = Boolean.FALSE;
    public BowAdapter(Bow bow) {
        this.bow = bow;
    }
    @Override
    public String normalAttack() {
        aimShoot = Boolean.FALSE;
        return bow.shootArrow(aimShoot);
    }

    @Override
    public String chargedAttack() {
        System.out.println(aimShoot);
        if(aimShoot.equals(Boolean.FALSE)){
            this.aimShoot = Boolean.TRUE;
            return "Entered aim shot mode";
        }else{
            return bow.shootArrow(aimShoot);
        }

    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        // TODO: complete me
        return bow.getHolderName();
    }
}
