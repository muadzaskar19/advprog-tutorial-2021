package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    Boolean kuat = Boolean.FALSE;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
    }

    @Override
    public String normalAttack() {
        kuat = Boolean.FALSE;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if(kuat.equals(Boolean.FALSE)){
            this.kuat = Boolean.TRUE;
            return spellbook.largeSpell();
        }else{
            return "Magic power not enough for large spell";
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
