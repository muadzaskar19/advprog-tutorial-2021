package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class AdventurerCalculatorServiceImpl implements AdventurerCalculatorService {

    @Override
    public String countPowerPotensialFromBirthYear(int birthYear) {
        int rawAge = getRawAge(birthYear);
        String kelas = "";
        if (rawAge<30) {
            if(rawAge*2000 <= 20000){
                kelas = " C class";
            }else if(rawAge*2000 <= 100000){
                kelas = " B class";
            }else{
                kelas = " A class";
            }
            return rawAge*2000+ kelas;
        } else if (rawAge <50) {
            if(rawAge*2250 <= 20000){
                kelas = " C class";
            }else if(rawAge*2250 <= 100000){
                kelas = " B class";
            }else{
                kelas = " A class";
            }
            return rawAge*2250+ kelas;
        } else {
            if(rawAge*5000 <= 20000){
                kelas = " C class";
            }else if(rawAge*5000 <= 100000){
                kelas = " B class";
            }else{
                kelas = " A class";
            }
            return rawAge*5000+ kelas;
        }
    }

    private int getRawAge(int birthYear) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear-birthYear;
    }
}
